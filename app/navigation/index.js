import { createStackNavigator } from 'react-navigation';
import ProductScreen from '../components/Product/Product';
import ProductDetail from '../components/ProductDetail/ProductDetail';


//Initial route is Product Screen
export const RootStack = createStackNavigator({
    Product: {
      screen: ProductScreen,
      navigationOptions : {
        header : null
      }
    },
    ProductDetail: {
      screen: ProductDetail,
      navigationOptions : {
        header : null
      }
    },
  });