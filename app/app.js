import React, { Component } from 'react';
import { RootStack } from './navigation';
import { View } from 'react-native';

export default class App extends Component {
  render() {
    //Navigation of app  
    return (
      <View style={{ flex: 1 }}>
        <RootStack />
      </View>
    );
  }
}