import React from 'react';
import { Text, View, Platform, Image } from 'react-native';
import {
    HeaderView,
    HeaderLeft,
    HeaderTitle,
    HeaderText,
    HeaderRight
} from './style.js'

import Touchable from '../touchable/touchable'

export const Header = ({ headerText , onBackPress }) => {
    return (
        <HeaderView>
            <HeaderLeft theme={{ opacity: 1 }}>
                <Touchable onPress={onBackPress}>
                    <Image
                        resizeMode="contain"
                        source={require('../../assets/back.png')}
                        style={{ width: 20, height: 20 }} />
                </Touchable>
            </HeaderLeft>
            <HeaderTitle>
                <View>
                    <Text style={{ fontSize: 17, color: '#62626f', fontWeight: '500',fontFamily: 'Montserrat-Regular' }}>{headerText}</Text>
                </View>
            </HeaderTitle>
            <HeaderRight>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                    <Touchable>
                        <Image
                            resizeMode="contain"
                            source={require('../../assets/search.png')}
                            style={{ width: 20, height: 20 }} />
                    </Touchable>
                </View>
            </HeaderRight>
        </HeaderView>

    );
};