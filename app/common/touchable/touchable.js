import { TouchableOpacity } from 'react-native';
import React from 'react';

//functional component
const Touchable = ({onPress,children , style}) => {
    return (
        <TouchableOpacity onPress={onPress} style={style}>
            {children}
        </TouchableOpacity>
    );
}

export default Touchable;