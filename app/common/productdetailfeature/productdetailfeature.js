import React from 'react';
import { Text, View, Platform, Image } from 'react-native';
import { Item, Title, BottomBox } from './style';

//functional component 
export const ProductDetailFeature = ({ feature, detail }) => {
    return (
        <Item>
            <BottomBox>
                <View style={{ flex: .5, justifyContent: 'center' }}>
                    <Title>{feature}</Title>
                </View>
                <View style={{ flex: .5, alignItems: 'flex-end', justifyContent: 'center' }}>
                    <Title>{detail}</Title>
                </View>
            </BottomBox>
        </Item>

    );
};