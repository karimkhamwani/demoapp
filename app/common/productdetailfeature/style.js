import styled from 'styled-components/native';


export const Item = styled.View`
height: 60
borderBottomWidth: 1
borderBottomColor: lightgray
`;


export const Title = styled.Text`
color: #48484a
 fontSize: 20 
 fontFamily: Montserrat-Regular
`;




export const BottomBox = styled.View`
flex: 1
flexDirection: row
 marginHorizontal: 10 
`;