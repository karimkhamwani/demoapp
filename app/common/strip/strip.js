import React from 'react';
import { Text, View, Platform, Image } from 'react-native';
import * as Animatable from 'react-native-animatable';


// functional component
export const Strip = ({ label }) => {
    return (
       
        <View style={{flex:1 , backgroundColor : "#f37479" , justifyContent:'center' , alignItems:'center'}}>
             {/* Animate Text inside Strip */}
             <Animatable.View 
                    animation="bounceInLeft"
                    easing="ease"
                    duration={1000}
                    iterationCount={1}>
            <Text style={{color:'white' , fontSize:20 , fontFamily:'Pacifico'}}>{label}</Text>
            </Animatable.View>
        </View>
        

    );
};