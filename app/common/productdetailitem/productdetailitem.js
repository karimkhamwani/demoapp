import { View, Text, Image, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import React from 'react';
import { ImageBox, InnerBox, Star, EuroIcon, PriceValue, ProductContent } from './style';
import { DetailBoxLeft, Div, DetailBoxTitle, ProductName, DetailBox, Description } from '../../components/ProductDetail/style';



//functional component
const ProductDetailItem = ({ itemImage, itemName, navigation }) => {
  return (

    <View style={{ flex: 1, backgroundColor: 'transparent' }}>
      <ImageBackground
        style={{ flex: 1 }}
        source={itemImage}>
        <TouchableWithoutFeedback onPress={() => navigation.navigate('ProductDetail')}>
          <ImageBox>
            <DetailBoxLeft>
              <Div>
                <InnerBox>
                  <Div>
                    <DetailBoxLeft>
                      <Star>
                        <Image
                          resizeMode={'contain'}
                          style={{ width: 30, height: 30, left: 10, top: 5 }}
                          source={require('../../assets/shape.png')} />
                      </Star>
                      <DetailBoxTitle>
                        <ProductName>
                          {itemName}
                        </ProductName>
                      </DetailBoxTitle>
                      <View style={{ flex: .3 }}>
                        <DetailBox>
                          <EuroIcon>
                            <Image
                              resizeMode={'contain'}
                              style={{ width: 30, height: 30, left: 10, tintColor: '#f37479' }}
                              source={require('../../assets/euro.png')} />

                          </EuroIcon>
                          <PriceValue>
                            <Text style={{ color: '#f37479', fontSize: 17, fontWeight: '500' }}>
                              400
                        </Text>
                          </PriceValue>
                        </DetailBox>
                      </View>
                    </DetailBoxLeft>

                  </Div>
                </InnerBox>
              </Div>
              <ProductContent>
                <View style={{ flex: 1, padding: 20, top: 30 }}>
                  <Description>
                    Lorem ipsum dolor sit amet, consect adipiscing elit, sed do eiusm temp incididunt ut labore et dolores atin magna aliqua.
                  </Description>
                </View>
              </ProductContent>
            </DetailBoxLeft>
          </ImageBox>
        </TouchableWithoutFeedback>
      </ImageBackground>
    </View>

  );
}

export default ProductDetailItem;