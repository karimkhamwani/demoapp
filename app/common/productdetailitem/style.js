import styled from 'styled-components/native';


export const ImageBox = styled.View`
flex: 1
borderRadius: 5 
`;

export const InnerBox = styled.View`
flex: 1
flexDirection: row
`;



export const Star = styled.View`
flex: .3
justifyContent:center
`;


export const EuroIcon = styled.View`
flex: .2
justifyContent:center
`;

export const PriceValue = styled.View`
flex: .8
justifyContent:center
`;

export const ProductContent = styled.View`
flex: .5
backgroundColor: white
`;



