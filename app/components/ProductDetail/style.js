import styled from 'styled-components/native';


export const DetailBox = styled.View`
flex: 1 
flexDirection:row 
`;

export const Div = styled.View`
flex: .5 
`;


export const DetailBoxLeft = styled.View`
flex: 1
flexDirection:column 
`;

export const Star = styled.View`
flex: .3 
justifyContent: flex-end 
`;

export const DetailBoxTitle = styled.View`
flex: .4 
justifyContent: center
left: 10
`;

export const ProductName = styled.Text`
color: #747476
fontSize: 22
fontWeight: 300 
fontFamily: Montserrat-Regular
`;

export const PriceBox = styled.View`
flex: 1 
flexDirection:row
`;

export const Euro = styled.View`
flex: .2
justifyContent:flex-start
`;


export const Price = styled.View`
flex: .8
justifyContent: flex-start
`;

export const Value = styled.Text`
color: #f37479 
fontSize: 20 
fontWeight:500 
top: 2
`;


export const ProductContent = styled.View`
flex: .2 
backgroundColor:white
`;

export const Description = styled.Text`
color: #aaaab1
fontSize: 18
fontWeight: 500
fontFamily: Montserrat-Regular
`;

export const FeatureBox = styled.View`
flex: .4 
marginVertical: 10
marginHorizontal: 10 
`;




export const ShadedBox = styled.View`
backgroundColor: white
flex: 1 
flexDirection: column
borderWidth: 1
shadowColor: #000
shadowOpacity: 0.1
borderColor: lightgray
`;