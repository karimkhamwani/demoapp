import React, { Component } from 'react';
import { View, Image, Text, ImageBackground } from 'react-native';
import { Header } from '../../common/header/header';
import { Strip } from '../../common/strip/strip';
import { ProductDetailFeature } from '../../common/productdetailfeature/productdetailfeature';
import { DetailBox, Div, DetailBoxLeft, Star, DetailBoxTitle, ProductName, PriceBox, Euro, Price, Value, ProductContent, Description, FeatureBox, ShadedBox } from './style';



class ProductDetail extends Component {
    state = {}
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <Header headerText={"Product"} onBackPress={() => this.props.navigation.goBack()} />
                <View style={{ flex: 1 }}>
                    <View style={{ flex: .1 }}>
                        <Strip label={'Dips & Onebites'} />
                    </View>
                    <View style={{ flex: .3 }}>
                            <ImageBackground
                                style={{ flex: 1, height: 300 }}
                                source={require('../../assets/cream.png')}>
                                <DetailBox>
                                    <Div>
                                        <DetailBoxLeft>
                                            <Star>
                                                <Image
                                                    resizeMode={'contain'}
                                                    style={{ width: 30, height: 30, left: 10 }}
                                                    source={require('../../assets/shape.png')} />
                                            </Star>
                                            <DetailBoxTitle>
                                                <ProductName>
                                                    {'Beet & Calery'}
                                                </ProductName>
                                            </DetailBoxTitle>
                                            <View style={{ flex: .3 }}>
                                                <PriceBox>
                                                    <Euro>
                                                        <Image
                                                            resizeMode={'contain'}
                                                            style={{ width: 30, height: 30, left: 10, tintColor: '#f37479' }}
                                                            source={require('../../assets/euro.png')} />
                                                    </Euro>
                                                    <Price>
                                                        <Value>
                                                            400
                                                        </Value>
                                                    </Price>
                                                </PriceBox>
                                            </View>
                                        </DetailBoxLeft>
                                    </Div>
                                </DetailBox>
                            </ImageBackground>
                    </View>
                    <ProductContent>
                        <View style={{ flex: 1, padding: 20 }}>
                            <Description>
                                Lorem ipsum dolor sit amet, consect adipiscing elit, sed do eiusm temp incididunt ut labore et dolores atin magna aliqua.
                        </Description>
                        </View>
                    </ProductContent>
                    <FeatureBox>
                        <ShadedBox>
                            <ProductDetailFeature feature={'Energy'} detail={'2404kJ (577kcal)'} />
                            <ProductDetailFeature feature={'Fat'} detail={'39g'} />
                            <ProductDetailFeature feature={'Carbohydrates'} detail={'45g'} />
                            <ProductDetailFeature feature={'Protein'} detail={'55g'} />
                        </ShadedBox>
                    </FeatureBox>
                </View>
            </View>
        );
    }
}



export default ProductDetail;