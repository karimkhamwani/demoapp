import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import { Header } from '../../common/header/header';
import Swiper from 'react-native-swiper';
import Carousel from 'react-native-snap-carousel';
import ProductDetailItem from '../../common/productdetailitem/productdetailitem';
import * as Animatable from 'react-native-animatable';


//set static data
const category = [
  { image: require('../../assets/cream.png'), name: 'Beet & Callerry' },
  { image: require('../../assets/cake1.png'), name: 'Coco Crumbs' },
  { image: require('../../assets/juice1.png'), name: 'Pine Cherry' },
  { image: require('../../assets/drink1.png'), name: 'Salad' },

]

class ProductScreen extends Component {

  _renderItem = ({ item, index }) => {
    return (
      <ProductDetailItem itemImage={item.image} itemName={item.name} navigation={this.props.navigation} />
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header
          headerText="Products" />
        <View style={{ flex: .4 }}>
          <View style={{ flex: 1 }}>
            <Swiper style={styles.wrapper}
              activeDotColor="white"
              ref='mySwiper'
              paginationStyle={{ top: -100 }}
              showsButtons={false}>
              <View style={styles.slide1}>
                <Animatable.View
                  ref={ref => { this.animatable = ref }}
                  easing="ease"
                  iterationCount={1}>
                  <Text style={styles.text}>Dips & Onebits</Text>
                </Animatable.View>
              </View>
              <View style={styles.slide2}>
                <Animatable.View
                  ref={ref => { this.animatable1 = ref }}
                  easing="ease"
                  iterationCount={1}>
                  <Text style={[styles.text, { color: '#5e7a96' }]}>Bars & Snacks</Text>
                </Animatable.View>
              </View>
              <View style={styles.slide3}>
                <Animatable.View
                  ref={ref => { this.animatable2 = ref }}
                  easing="ease"
                  iterationCount={1}>
                  <Text style={styles.text}>Drinks</Text>
                </Animatable.View>
              </View>
              <View style={styles.slide4}>
                <Animatable.View
                  ref={ref => { this.animatable3 = ref }}
                  easing="ease"
                  iterationCount={1}>
                  <Text style={styles.text}>Salads</Text>
                </Animatable.View>
              </View>
            </Swiper>
          </View>
        </View>
        <View style={{ flex: .6, alignItems: 'center' }}>
          <View style={{ height: 400, width: 350, position: 'absolute', zIndex: 9999, top: -100 }}>
            <Carousel
              layout={'stack'}
              data={category}
              horizontal={true}
              onSnapToItem={(index) => {
                //scroll swiper by one
                this.refs.mySwiper.scrollBy(1)
                //animate heading
                this.animatable.fadeIn(1500)
                this.animatable1.fadeIn(1500)
                this.animatable2.fadeIn(1500)
                this.animatable3.fadeIn(1500)
                
              }}
              renderItem={this._renderItem}
              itemWidth={Platform.OS == 'ios' ? 310 : 350}
              sliderWidth={Platform.OS == 'ios' ? 350 : 400}
            // android sliderWidth 400 ios 350 itemWidth 350 or ios 310
            />
          </View>
        </View>
      </View>
    );
  }
}





export default ProductScreen

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#f37479'
  },
  slide2: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#d0e7fc',
  },
  slide3: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#8d5ea7',
  },
  slide4: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ff9575',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    top: 70,
    fontWeight: 'bold',
    fontFamily: 'Pacifico'
  }
})