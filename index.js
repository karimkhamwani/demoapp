/** @format */

import {AppRegistry} from 'react-native';
import App from './app/app';

import {name as appName} from './app.json';

//Starting point of app
AppRegistry.registerComponent(appName, () => App);
